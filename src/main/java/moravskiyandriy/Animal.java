package moravskiyandriy;

public class Animal {
    protected String name;

    protected String food;

    protected String sleep;

    public Animal(String name) {
        this.name = name;
    }

    public Animal(String name, String food) {
        this.name = name;
        this.food = food;
    }

    public Animal(String name, String food, String sleep) {
        this.name = name;
        this.food = food;
        this.sleep = sleep;
    }

    protected String getFood() {
        return food;
    }

    protected void setFood(String food) {
        this.food = food;
    }

    protected String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    protected String getSleep() {
        return sleep;
    }

    protected void setSleep(String sleep) {
        this.sleep = sleep;
    }

    protected void sleep() {
        System.out.println("Sleeps " + sleep);
    }

    protected void eat() {
        System.out.println("Eats " + food);
    }

    protected void name() {
        System.out.println("Name is " + name);
    }

    public void getFullInfo() {
        System.out.println("This animal's name is " + getName() + ". It usually sleeps " + sleep + ". Favourite food of " + name + " is " + food + ".");
    }
}
