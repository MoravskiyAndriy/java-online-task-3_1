package moravskiyandriy;

public class Fish extends Animal {
    public Fish(String name) {
        super(name);
    }

    public Fish(String name, String food) {
        super(name, food);
    }


    public Fish(String name, String food, String sleep) {
        super(name, food, sleep);
    }

    @Override
    protected void sleep() {
        System.out.println(name + " sleeps " + sleep);
    }

    @Override
    protected void eat() {

        System.out.println(name + "eats " + food);
    }

    @Override
    protected void name() {

        System.out.println("fish's name is " + name);
    }

    public void swim() {
        System.out.println("Can swim");
    }

    @Override
    public void getFullInfo() {
        System.out.println("This fish's name is " + getName() + ". It usually sleeps " + sleep + ". Favourite food of " + name + " is " + food + ".");
    }
}
