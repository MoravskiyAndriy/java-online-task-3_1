package moravskiyandriy;

public class Bird extends Animal {
    public Bird(String name) {
        super(name);
    }

    public Bird(String name, String food) {
        super(name, food);
    }

    public Bird(String name, String food, String sleep) {
        super(name, food, sleep);
    }

    public void fly() {
        System.out.println("Can fly");
    }

    @Override
    protected void sleep() {
        System.out.println(name + " sleeps " + sleep);
    }

    @Override
    protected void eat() {

        System.out.println(name + "eats " + food);
    }

    @Override
    protected void name() {

        System.out.println("Bird's name is " + name);
    }

    @Override
    public void getFullInfo() {
        System.out.println("This bird's name is " + getName() + ". It usually sleeps " + sleep + ". Favourite food of " + name + " is " + food + ".");
    }
}
