package moravskiyandriy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Task3 {
    public static void main(String[] args) {
        List<Animal> list = new ArrayList<Animal>();
        Bird eagle = new Bird("Marvin", "meat", "at night");
        Bird crow = new Bird("Dexter", "grain", "at night");
        Fish barbus = new Fish("Hunter", "meat", "at night");
        Fish clown = new Fish("Bingo", "algae", "at daytime");
        list.add(eagle);
        list.add(crow);
        list.add(barbus);
        list.add(clown);
        for (Animal a : list) {
            a.name();
            a.sleep();
            a.eat();
            System.out.println();
        }
        System.out.println();
        System.out.println("Sorted by name:");
        Collections.sort(list, new NameComparator());
        for (Animal a : list) {
            a.getFullInfo();
        }
    }
}
